var express = require('express');
var app = express();
var MongoClient = require('mongodb').MongoClient;
var bodyParser = require('body-parser');
const cookieParser = require("cookie-parser");
const session = require("express-session");

app.use(bodyParser.json());
app.use(session({
    secret: 'LONG_RANDOM_STRING_HERE',
    resave: true,
    saveUninitialized: false
}));
app.use(cookieParser());

const url = 'mongodb://localhost:27017';
const mongoClient = new MongoClient(url, {useNewUrlParser: true});

const dbName = 'uds';

var news, comments, tags, archives;


async function getComments(id) {
    return comments.find({ article_id: +id }).toArray()
}


// Use connect method to connect to the server
mongoClient.connect(function (err, client) {
    console.log("Connected successfully to server");

    const db = client.db(dbName);


    news = db.collection('news');
    comments = db.collection('comments');
    tags = db.collection('tags');
    archives = db.collection('archives');


});

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8099");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Cookie");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, PATCH, OPTIONS");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

app.get('/getNews', function (req, res) {
    let newsObj = {};
    news.find({}).toArray(async function (err, docs) {
        newsObj = docs;
        newsObj = await Promise.all(newsObj.map(async (article) => ({
          ...article,
          comments: await getComments(article._id)
        })));

        res.send(newsObj)
    });

});


app.get('/getTags', function (req, res) {
    tags.find({}).toArray(function (err, docs) {
        res.send(docs)
    })
});

app.get('/getArchives', function (req, res) {
    archives.find({}).toArray(function (err, docs) {
        res.send(docs)
    })
});

app.get('/getComments', function (req, res) {
    comments.find({article_id: +req.query.id}).toArray(function (err, docs) {
        res.send(docs);
    })
});


app.post('/addComment', function (req, res) {


    comments.find().toArray(function (er, docs) {
        const id = docs[docs.length - 1]._id + 1;

        const newComment = {
            _id: id,
            article_id: +req.query.id,
            author: req.body.author,
            date: "2 мая 2020 года",
            text: req.body.text,
            likes: 0,
            isLiked: false
        };

        comments.insert(newComment, function (err, record) {
            if (!err) {
                comments.find({article_id: +req.query.id}).toArray(function (e, docss) {
                    res.send(docss)
                })
            } else {
                res.send(err)
            }
        });
    })
});

app.get('/getArticle', function (req, res) {
    news.find({_id: +req.query.id}).toArray(function (err, docs) {
        res.send(docs[0]);
    });
});

app.patch('/editArticle', function (req, res) {
    const likesIncrease = req.body.isLiked ? 1 : -1;
    news.findOneAndUpdate(
        {_id: +req.query.id},
        {
            $inc: {
                likes: likesIncrease
            },
            $set: {
                isLiked: req.body.isLiked
            }
        },
        {returnOriginal: false},
        function (err, docs) {
            res.send(docs.value)
        })
});

app.patch('/editComment', function (req, res) {
    const likesIncrease = req.body.isLiked ? 1 : -1;
    comments.findOneAndUpdate(
        {_id: +req.query.commentId},
        {
            $inc: {
                likes: likesIncrease
            },
            $set: {
                isLiked: req.body.isLiked
            }
        },
        {returnOriginal: false},
        function (err, docs) {
            comments.find({article_id: +req.query.articleId}).toArray(function (er, docss) {
                res.send(docss)
            })
        })
});

app.get("/workflow", require('./session'));


app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
